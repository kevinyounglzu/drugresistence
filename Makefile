CPPFLAGS=-Iinclude -std=c++11 -g -Wall -Wextra

UNAME := $(shell uname)
# my cpp lib
ifeq ($(UNAME), Darwin)
  MYLIB_PATH=$(HOME)/working/mycpplib
else ifeq ($(UNAME), Linux)
  MYLIB_PATH=$(HOME)/libs/cpplib
endif

MYLIB_I=$(MYLIB_PATH)/include
MYLIB_L=$(MYLIB_PATH)/lib
MYLIB=mycpp
MYLIB_FLAG=-I$(MYLIB_I) -L$(MYLIB_L)

# snap lib
ifeq ($(UNAME), Darwin)
  SNAP_PATH=$(HOME)/working/snap
  LIBS=
else ifeq ($(UNAME), Linux)
  SNAP_PATH=$(HOME)/libs/snap
  LIBS= -lrt
endif

SNAP_CORE=$(SNAP_PATH)/snap-core
SNAP_GLIB=$(SNAP_PATH)/glib-core
SNAP_LIB_L=$(SNAP_CORE)
SNAP_LIB=snap


SNAP_FLAG=-isystem$(SNAP_CORE) -isystem$(SNAP_GLIB) -L$(SNAP_LIB_L)

# for test
GTEST=-lgtest

TEST_SRC=$(wildcard test/*.cpp)
TEST_OBJ=$(patsubst %.cpp,%.o,$(TEST_SRC))
TEST=test/test
.PONY: tests clean run cleandata cleanall cleanconfig config


RUN_SRC=src/smallw.cpp
RUN=$(patsubst %.cpp,%,$(RUN_SRC))
RUN_OBJ=$(patsubst %.cpp,%.o,$(RUN_SRC))

all: tests

run: CPPFLAGS+=$(MYLIB_FLAG) $(SNAP_FLAG) -O3
run: $(RUN)

$(RUN): $(RUN_SRC)
	$(CXX) $(CPPFLAGS) $< -o $@ -l$(MYLIB) -l$(SNAP_LIB) $(LIBS)


diag:
	@python logana.py

break:
	@python logana.py 1

LOGFILE=./prun.log
CONFIGFILE=./config/meta.cfg
JOBS=-j4

prun: $(RUN)
	parallel --progress --joblog $(LOGFILE) $(JOBS) --colsep ' ' ./$(RUN) :::: $(CONFIGFILE)

prundry: $(RUN)
	parallel --dry-run --progress --joblog $(LOGFILE) $(JOBS) --colsep ' ' ./$(RUN) :::: $(CONFIGFILE)


tests: CPPFLAGS+=$(GTEST) $(SNAP_FLAG) $(MYLIB_FLAG) -l$(MYLIB) -l$(SNAP_LIB)
tests: $(TEST)
	./$(TEST)

#$(TEST): CPPFLAGS+=-I$(MYLIB_I)
$(TEST): $(TEST_OBJ) $(HEADERS)
	$(CXX) $(CPPFLAGS) $(TEST_OBJ) -o $@

cleanconfig:
	rm -rf ./config/*.cfg

genconfig:
	mkdir -p config
	python configgen.py
	@echo "`ls config | wc -l` files generated"

clean:
	rm -rf $(TEST) $(TEST_OBJ)
	rm -rf $(RUN) $(RUN_OBJ)
	rm -rf `find . -name "*.dSYM" -print`

cleanall: clean
	rm -rf data/*

cleandata:
	rm -rf data/*
