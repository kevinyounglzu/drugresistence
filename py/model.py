# -*- coding: utf8 -*-
import random


class Patch(object):
    """
    The patch object.
    """
    def __init__(self, parameters, number, drug_level, initial_number):
        """
        self.N : the carrying capacity of the patch
        self.death_rate : the death rate of the agents here
        self.migration_rate : the migration rate of the agents here
        self.mutation_rate : the mutation rate of the agents here
        self.drug_level : the antibiotic level of the patch
        self.upper_drug_level : the upper bound of the resistence level agents
            can have
        self.initial_number : the initial numer of agents of the patch
        self.agents : the list to store all the agents, index stands for the drug
            resistence level
        """
        self.parameters = parameters
        self.N = self.parameters["N"]
        self.death_rate = self.parameters["death_rate"]
        self.mutation_rate = self.parameters["mutation_rate"]
        self.upper_drug_level = self.parameters["upper_drug_level"]

        self.number = number
        self.drug_level = drug_level
        self.initial_number = initial_number

        # initiate the list to hold all the agents
        # the drug resistence of the agents starts from 0
        self.agents = []
        for i in range(self.upper_drug_level):
            self.agents.append(0)
        self.agents[self.drug_level] = self.initial_number

    @property
    def emptyNumber(self):
        return self.N - sum(self.agents)

    def getRandomAgentLevel(self):
        index = random.randint(0, self.N-1)
        #print "The index", index
        for i in range(self.upper_drug_level):
            index -= self.agents[i]
            if index < 0:
                return i
        return -1

    def pushAgent(self, the_level):
        if the_level > -1 and the_level < self.upper_drug_level:
            self.agents[the_level] += 1

    def popAgent(self, the_level):
        if the_level > -1 and the_level < self.upper_drug_level:
            assert self.agents[the_level] > 0
            self.agents[the_level] -= 1

    def grow(self):
        """
        Method to grow the whole population according to logistic model
        """

        # find an agent randomly
        the_level = self.getRandomAgentLevel()
        # make sure it's not empty
        if the_level > -1:
            # find an empty site
            if random.random() < (self.emptyNumber/float(self.N)):
                # if there is an empty site, duplicate
                self.pushAgent(the_level)

    def die(self):
        """
        Method to kill agents
        """
        if random.random() < self.death_rate:
            self.popAgent(self.getRandomAgentLevel())

    def mutate(self):
        """
        The method for mutation
        """
        # try to mutate
        if random.random() < self.mutation_rate:
            # find an agent randomly
            the_level = self.getRandomAgentLevel()
            # make sure it's not empty
            if the_level > -1:
                self.popAgent(the_level)
                # try to go -1
                if random.random() < 0.5:
                    if the_level > self.drug_level:
                        self.pushAgent(the_level-1)
                else:
                    # make sure the agent's resistence is below the upper bound
                    if the_level < self.upper_drug_level - 1:
                        self.pushAgent(the_level+1)
                    else:
                        self.pushAgent(the_level)


class System(object):
    def __init__(self, L, N, death_rate, migration_rate, mutation_rate, upper_drug_level):
        """
        self.L : the length of the system
        """
        self.L = L
        self.migration_rate = migration_rate
        self.parameters = {
            "N": N,
            "death_rate": death_rate,
            "mutation_rate": mutation_rate,
            "upper_drug_level": upper_drug_level
        }

        self.patches = []
        for i in range(self.L):
            if i == 0:
                initial_number = self.parameters["N"]
            else:
                initial_number = 0
            self.patches.append(Patch(self.parameters, i, i, initial_number))

    def getRandomPatch(self):
        return random.randint(0, self.L-1)

    def findRandomNeighbor(self, l_index):
        if l_index == 0:
            return self.patches[1]
        elif l_index == self.L-1:
            return self.patches[self.L-2]
        else:
            if random.random() < 0.5:
                return self.patches[l_index-1]
            else:
                return self.patches[l_index+1]

    def migrate(self, l_index):
        if random.random() < self.migration_rate:
            # find an agent from that patch randomly
            the_patch = self.patches[l_index]
            n_index = the_patch.getRandomAgentLevel()
            # make sure the agent is not empty
            if n_index > -1:
                nei_patch = self.findRandomNeighbor(l_index)
                if nei_patch.emptyNumber > 0:
                    the_patch.popAgent(n_index)
                    if n_index >= nei_patch.drug_level:
                        nei_patch.pushAgent(n_index)

    def oneTimeStep(self):
        # die
        l_index = self.getRandomPatch()
        self.patches[l_index].die()

        # grow
        l_index = self.getRandomPatch()
        self.patches[l_index].grow()

        # mutate
        l_index = self.getRandomPatch()
        self.patches[l_index].mutate()

        # migrate
        l_index = self.getRandomPatch()
        self.migrate(l_index)

    def oneMCStep(self):
        for i in range(self.L * self.parameters["N"]):
            self.oneTimeStep()


if __name__ == "__main__":
    pass
