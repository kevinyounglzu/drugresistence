# -*- coding: utf8 -*-

import numpy as np


def genSimRegular():
    size = 300
    agent_n = 100
    degree = 6
    repeat = 1000
    steps = 1000
    # gen all configs

    cutoutp = 0.05

    r = 0.2

    mus = np.linspace(0, 0.25, 41)
    ms = mus * r

    cs = np.linspace(0., 1.0, 201)

    counter = 0
    for m in ms:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)
                f.write("int agent_n=%d\n" % agent_n)
                f.write("int steps=%d\n" % steps)
                f.write("double r=%f\n" % r)
                f.write("double m=%f\n" % m)
                f.write("double c=%f\n" % c)
                f.write("int repeat=%d\n" % repeat)
                f.write("double cutoutp=%f\n" % cutoutp)
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


if __name__ == "__main__":
    genSlowTri()
