#ifndef ALLEEDIFFUSION_H
#define ALLEEDIFFUSION_H

#include "snaphelper.h"
#include "patch.h"
#include <iostream>
#include "rand.h"
#include <cassert>

class AlleeDiffusionBase
{
    public:
        typedef Network<AlleePatch> NetType;
    protected:
        int patch_capa;
        double growth_rate;
        double allee_threshold;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::RandInt rand_agent;
        mylib::RandDouble randd;
    public:
        AlleeDiffusionBase(int n, double r, double k, PUNGraph graph):
            patch_capa(n),
            growth_rate(r),
            allee_threshold(k),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            rand_agent(1, patch_capa),
            randd()
        {
        }

        virtual void diffuse() = 0;

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatch & patch = node_iter.getNode();
                patch.setCapa(patch_capa);
                patch.setGrowthRate(growth_rate);
                patch.setAlleeK(allee_threshold);
                patch.setF0(0);
            }
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
//                writeDist(out);
                out << getOcupition() << endl;
                if(verbose)
                    cout << i << " steps.." << endl;
            }
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa*net_size; ++i)
            {
                oneTimeStep();
            }
        }
        
        virtual void oneTimeStep()
        {
            birthDeath();
            diffuse();
        }

        virtual void birthDeath()
        {
            // choose a patch randomly
            net.getNodeFromId(rand_patch.gen()).getNode().birthDeathProcess();
        }

        virtual void writeDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter.getNode().getPercent() << " ";
            out << endl;
        }

        virtual void getDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter->GetId() << " " << node_iter.getNode().getPercent() << endl;
        }

        virtual double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().getPercent();
            return base;
        }

};

class ConstDiffuse: public AlleeDiffusionBase
{
    protected:
        double diffusion_rate;
    public:
        ConstDiffuse(int n, double r, double k, double m, PUNGraph graph):
            AlleeDiffusionBase(n, r, k, graph),
            diffusion_rate(m)
        {
            initialize();
        }

        virtual void initialize()
        {
            AlleeDiffusionBase::initialize();
            net.getNodeFromId(rand_patch.gen()).getNode().setF0(1);
        }

        virtual void diffuse()
        {
            // choose a node randomly
            NetType::NodeIterator node_iter = net.getNodeFromId(rand_patch.gen());
            // get the node itself
            AlleePatch & patch = node_iter.getNode();

            // choose an agent from the patch randomly
            if(rand_agent.gen()<= patch.getNumber())
            {
                // decide if to diffuse
                if(randd.gen() < diffusion_rate)
                {
                    // diffuse
                    patch.popOne();
                    // choose a neighor to go
                    int number_of_neighbor = node_iter->GetDeg();
                    if(number_of_neighbor>0)
                    {
                        net.getNodeFromId(
                                node_iter->GetNbrNId(rand_patch.genOTF(0, number_of_neighbor-1))
                                ).getNode().pushOne();
                    }
                }
            }
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
//            out << net_size << " "
//                << patch_capa << " "
//                << growth_rate << " "
//                << allee_threshold << " "
//                << diffusion_rate << " ";
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
//                writeDist(out);
                out << getOcupition() << " ";
                if(verbose)
                    cout << i << " steps.." << endl;
            }
            out << endl;
        }

};

class AlleeDiffusion
{
    public:
        typedef Network<AlleePatchSim> NetType;
    public:
        int patch_capa;
        double growth_rate;
        double allee_threshold;
        double diffusion_rate;
        int net_size;
        double cutoutp;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::RandDouble randd;
        mylib::SimWatcherConverge swc;
    public:
        AlleeDiffusion(int n, double r, double c, double m, double cutoutpp, PUNGraph graph, int steps, double epsilon, double init):
            patch_capa(n),
            growth_rate(r),
            allee_threshold(c),
            diffusion_rate(m),
            net_size(graph->GetNodes()),
            cutoutp(cutoutpp * net_size),
            net(graph),
            rand_patch(0, net_size-1),
            randd(),
            swc(steps, epsilon, init)
            {
                initialize();
            }

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchSim & patch = node_iter.getNode();
                patch.setCapacity(patch_capa);
                patch.setGrowthRate(growth_rate);
                patch.setAlleeThreshold(allee_threshold);
                patch.setInitialPercent(0);
            }

            // choose one and fill up
            net.getNodeFromId(rand_patch.gen()).getNode().setInitialPercent(1);
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
                double ocupition = getOcupition();
//                writeDist(out);
                out << ocupition << std::endl;
                if(verbose)
                    cout << i << " steps.." << endl;
                if(ocupition > cutoutp)
                    break;
                if(swc.pushValue(ocupition))
                    break;
            }
        }

        virtual void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneMCStep();
                double ocupition = getOcupition();
                if(ocupition > cutoutp)
                    break;
                if(swc.pushValue(ocupition))
                    break;
            }
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa*net_size; ++i)
            {
                oneTimeStep();
            }
        }
        
        virtual void oneTimeStep()
        {
            birthDeath();
            diffuse();
        }

        virtual void birthDeath()
        {
            // choose a patch randomly
            net.getNodeFromId(rand_patch.gen()).getNode().birthDeathProcess();
        }

        virtual void diffuse()
        {
            // choose a node randomly
            NetType::NodeIterator node_iter = net.getNodeFromId(rand_patch.gen());
            // get the node itself
            AlleePatchSim & patch = node_iter.getNode();

            // choose an agent from the patch randomly
            if(randd.gen() < patch.getPercent())
            {
                // decide if to diffuse
                if(randd.gen() < diffusion_rate)
                {
                    // diffuse
                    patch.popOne();
                    // choose a neighor to go
                    int number_of_neighbor = node_iter->GetDeg();
                    if(number_of_neighbor>0)
                    {
                        net.getNodeFromId(
                                node_iter->GetNbrNId(rand_patch.genOTF(0, number_of_neighbor-1))
                                ).getNode().pushOne();
                    }
                }
            }
        }

        virtual void writeDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter.getNode().getPercent() << " ";
            out << endl;
        }

        virtual void getDist(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                out << node_iter->GetId() << " " << node_iter.getNode().getPercent() << endl;
        }

        virtual double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().getPercent();
            return base;
        }
};

// class to numerically solve the equation sets
class AlleeRK4
{
    public:
        typedef Network<AlleePatchRK4> NetType;

        double allee_threshold;
        double mu; // diffusion_rate / growth_rate
        double interval;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4(double k, double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            allee_threshold(k),
            mu(mu_ratio),
            interval(h),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().h = interval;
        }

        void seed()
        {
            getRandomNode().u = 1;
        }

        void seed(int id)
        {
            getNode(id).u = 1;
        }

        AlleePatchRK4 & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        AlleePatchRK4 & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                double ocupition = getOcupition();
                out << ocupition << " ";
                if(verbose)
                    std::cout << i * interval << std::endl;
                if(swc.pushValue(ocupition))
                    break;
            }
            out << endl;
        }

        void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                if(swc.pushValue(getOcupition()))
                    break;
            }
        }

        void dist(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                    out << node_iter.getNode().u << " ";
                out << endl;
                oneTimeStep();
            }
        }

        void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
        }

        void updatek1s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.k1 = patch.u * (1-patch.u) * (patch.u - allee_threshold) - mu * patch.u + mu * neighbor_flow;
            }
        }

        void updatek2s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k1 / 2.;
                patch.k2 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek3s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k2 / 2.;
                patch.k3 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek4s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k3;
                patch.k4 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;
            }
        }

        void updateus()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        bool guard()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().u;
            return base;
        }
};

// class to numerically solve the equation sets
class AlleeRK4TwoKind
{
    public:
        typedef Network<AlleePatchRK4TwoKind> NetType;

        double allee_threshold;
        double mu_u; // diffusion_rate / growth_rate
        double mu_v;
        double rho; // percentage for second kind
        double interval;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4TwoKind(double k, double mu_ratio_u, double mu_ratio_v, double ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            allee_threshold(k),
            mu_u(mu_ratio_u),
            mu_v(mu_ratio_v),
            rho(ratio),
            interval(h),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().h = interval;
        }

        void seed()
        {
            AlleePatchRK4TwoKind the_node = getRandomNode();
            the_node.u = 1 - rho;
            the_node.v = rho;
        }

        void seed(int id)
        {
            AlleePatchRK4TwoKind the_node = getNode(id);
            the_node.u = 1 - rho;
            the_node.v = rho;
        }

        AlleePatchRK4TwoKind & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        AlleePatchRK4TwoKind & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        void sim(int steps, std::ostream & out_u, std::ostream & out_v, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                std::pair<double, double> ocupition = getOcupition();
                out_u << ocupition.first << " ";
                out_v << ocupition.second << " ";
//                if(verbose)
//                    std::cout << i * interval << std::endl;
//                if(swc.pushValue(ocupition))
//                    break;
            }
            out_u << endl;
            out_v << endl;
        }

        void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
//                if(swc.pushValue(getOcupition()))
//                    break;
            }
        }

        void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
        }

        void updatek1s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4TwoKind & patch = node_iter.getNode();

                double neighbor_flow_u = 0;
                double neighbor_flow_v = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow_u += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                    neighbor_flow_v += neighbor_iter.getNode().v / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.u1 = patch.u * (1-patch.u-patch.v) * (patch.u + patch.v - allee_threshold) - mu_u * patch.u + mu_u * neighbor_flow_u;
                patch.v1 = patch.v * (1-patch.u-patch.v) * (patch.u + patch.v - allee_threshold) - mu_v * patch.v + mu_v * neighbor_flow_v;
            }
        }

        void updatek2s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4TwoKind & patch = node_iter.getNode();

                double neighbor_flow_u = 0;
                double neighbor_flow_v = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4TwoKind & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow_u += (neighbor_patch.u + interval * neighbor_patch.u1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                    neighbor_flow_v += (neighbor_patch.v + interval * neighbor_patch.v1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.u1 / 2.;
                double new_v = patch.v + interval * patch.v1 / 2.;
                patch.u2 = new_u * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_u * new_u + mu_u * neighbor_flow_u;
                patch.v2 = new_v * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_v * new_v + mu_v * neighbor_flow_v;

            }
        }

        void updatek3s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4TwoKind & patch = node_iter.getNode();

                double neighbor_flow_u = 0;
                double neighbor_flow_v = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4TwoKind & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow_u += (neighbor_patch.u + interval * neighbor_patch.u2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                    neighbor_flow_v += (neighbor_patch.v + interval * neighbor_patch.v2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.u2 / 2.;
                double new_v = patch.v + interval * patch.v2 / 2.;

                patch.u3 = new_u * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_u * new_u + mu_u * neighbor_flow_u;
                patch.v3 = new_v * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_v * new_v + mu_v * neighbor_flow_v;
            }
        }

        void updatek4s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4TwoKind & patch = node_iter.getNode();

                double neighbor_flow_u = 0;
                double neighbor_flow_v = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4TwoKind & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow_u += (neighbor_patch.u + interval * neighbor_patch.u3) / static_cast<double>(neighbor_iter->GetDeg());
                    neighbor_flow_v += (neighbor_patch.v + interval * neighbor_patch.v3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.u3;
                double new_v = patch.v + interval * patch.v3;
                patch.u4 = new_u * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_u * new_u + mu_u * neighbor_flow_u;
                patch.v4 = new_v * (1 - new_u - new_v) * (new_u + new_v - allee_threshold) - mu_v * new_v + mu_v * neighbor_flow_v;
            }
        }

        void updateus()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        bool guard()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        std::pair<double, double> getOcupition()
        {
            double base_u(0);
            double base_v(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                base_u += node_iter.getNode().u;
                base_v += node_iter.getNode().v;
            }
            return std::make_pair(base_u, base_v);
        }

        void dist(int steps, std::ostream & out_u, std::ostream & out_v)
        {
            for(int i=0; i<steps; ++i)
            {
                for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                {
                    out_u << node_iter.getNode().u << " ";
                    out_v << node_iter.getNode().v << " ";
                }
                out_u << endl;
                out_v << endl;
                oneTimeStep();
            }
        }
};

// Same to AlleeRK4, but the seed stay 1;
class AlleeRK4SolidSeed
{
    public:
        typedef Network<AlleePatchRK4> NetType;

        double allee_threshold;
        double mu; // diffusion_rate / growth_rate
        double interval;
        int seed_id;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4SolidSeed(double k, double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            allee_threshold(k),
            mu(mu_ratio),
            interval(h),
            seed_id(0),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().h = interval;
        }

        void seed()
        {
            seed_id = rand_patch.gen();
            reSeed();
        }

        void seed(int id)
        {
            seed_id = id;
            reSeed();
        }

        void reSeed()
        {
            getNode(seed_id).u = 1;
        }

        AlleePatchRK4 & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        AlleePatchRK4 & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                double ocupition = getOcupition();
                out << ocupition << " ";
                if(verbose)
                    std::cout << i * interval << std::endl;
                if(swc.pushValue(ocupition))
                    break;
            }
            out << endl;
        }

        void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                if(swc.pushValue(getOcupition()))
                    break;
            }
        }

        void dist(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                    out << node_iter.getNode().u << " ";
                out << endl;
                oneTimeStep();
            }
        }

        void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
            reSeed();
        }

        void updatek1s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.k1 = patch.u * (1-patch.u) * (patch.u - allee_threshold) - mu * patch.u + mu * neighbor_flow;
            }
        }

        void updatek2s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k1 / 2.;
                patch.k2 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek3s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k2 / 2.;
                patch.k3 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek4s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k3;
                patch.k4 = new_u * (1 - new_u) * (new_u - allee_threshold) - mu * new_u + mu * neighbor_flow;
            }
        }

        void updateus()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        bool guard()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().u;
            return base;
        }
};


/* Pure diffusion without any reaction. */
class AlleeRK4Diffusion
{
    public:
        typedef Network<AlleePatchRK4> NetType;

        double mu; // diffusion_rate / growth_rate
        double interval;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4Diffusion(double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            mu(mu_ratio),
            interval(h),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().h = interval;
        }

        void seed()
        {
            getRandomNode().u = 1;
        }

        void seed(int id)
        {
            getNode(id).u = 1;
        }

        AlleePatchRK4 & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        AlleePatchRK4 & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                double ocupition = getOcupition();
                out << ocupition << " ";
                if(verbose)
                    std::cout << i * interval << std::endl;
                if(swc.pushValue(ocupition))
                    break;
            }
            out << endl;
        }

        void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                if(swc.pushValue(getOcupition()))
                    break;
            }
        }

        void dist(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                    out << node_iter.getNode().u << " ";
                out << endl;
                oneTimeStep();
            }
        }

        void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
        }

        void updatek1s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.k1 = - mu * patch.u + mu * neighbor_flow;
            }
        }

        void updatek2s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k1 / 2.;
                patch.k2 = - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek3s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k2 / 2.;
                patch.k3 = - mu * new_u + mu * neighbor_flow;

            }
        }

        void updatek4s()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                AlleePatchRK4 & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    AlleePatchRK4 & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k3;
                patch.k4 = - mu * new_u + mu * neighbor_flow;
            }
        }

        void updateus()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        bool guard()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        double getOcupition()
        {
            double base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().u;
            return base;
        }
};


/* PatchType must have:
 * .u
 * .k1
 * .k2
 * .k3
 * .k4
 * .c
 * .h
 * .updateValue
 * */
template <typename PatchType>
class AlleeRK4VaryCBase
{
    public:
        typedef Network<PatchType> NetType;

        double mu; // diffusion_rate / growth_rate
        double interval;
        int net_size;
        NetType net;
        mylib::RandInt rand_patch;
        mylib::SimWatcherConverge swc;

        AlleeRK4VaryCBase(double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init):
            mu(mu_ratio),
            interval(h),
            net_size(graph->GetNodes()),
            net(graph),
            rand_patch(0, net_size-1),
            swc(steps, epsilon, init)
            { 
            }

        virtual void initialize() = 0;

        virtual void seed()
        {
            getRandomNode().u = 1;
        }

        virtual void seed(int id)
        {
            getNode(id).u = 1;
        }

        virtual PatchType & getNode(int id)
        {
            return net.getNodeFromId(id).getNode();
        }

        virtual PatchType & getRandomNode()
        {
            return getNode(rand_patch.gen());
        }

        virtual void sim(int steps, std::ostream & out, bool verbose=false)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                double ocupition = getOcupition();
                out << ocupition << " ";
                if(verbose)
                    std::cout << i * interval << std::endl;
                if(swc.pushValue(ocupition))
                    break;
            }
            out << endl;
        }

        virtual void sim(int steps)
        {
            for(int i=0; i<steps; ++i)
            {
                oneTimeStep();
                if(swc.pushValue(getOcupition()))
                    break;
            }
        }

        virtual void dist(int steps, std::ostream & out)
        {
            for(int i=0; i<steps; ++i)
            {
                for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                    out << node_iter.getNode().u << " ";
                out << endl;
                oneTimeStep();
            }
        }

        virtual void oneTimeStep()
        {
            updatek1s();
            updatek2s();
            updatek3s();
            updatek4s();
            updateus();
        }

        virtual void updatek1s()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                PatchType & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    typename NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    neighbor_flow += neighbor_iter.getNode().u / static_cast<double>(neighbor_iter->GetDeg());
                }

                patch.k1 = patch.u * (1-patch.u) * (patch.u - patch.c) - mu * patch.u + mu * neighbor_flow;
            }
        }

        virtual void updatek2s()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                PatchType & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    typename NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    PatchType & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k1 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k1 / 2.;
                patch.k2 = new_u * (1 - new_u) * (new_u - patch.c) - mu * new_u + mu * neighbor_flow;

            }
        }

        virtual void updatek3s()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                PatchType & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    typename NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    PatchType & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k2 / 2.) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k2 / 2.;
                patch.k3 = new_u * (1 - new_u) * (new_u - patch.c) - mu * new_u + mu * neighbor_flow;

            }
        }

        virtual void updatek4s()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                PatchType & patch = node_iter.getNode();

                double neighbor_flow = 0;
                // iterate through the neighbors add the flow
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    // get the neighbor iter and patch itself
                    typename NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    PatchType & neighbor_patch = neighbor_iter.getNode();

                    //calculate the flow
                    neighbor_flow += (neighbor_patch.u + interval * neighbor_patch.k3) / static_cast<double>(neighbor_iter->GetDeg());
                }

                double new_u = patch.u + interval * patch.k3;
                patch.k4 = new_u * (1 - new_u) * (new_u - patch.c) - mu * new_u + mu * neighbor_flow;
            }
        }

        virtual void updateus()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode().updateValue();
        }

        virtual bool guard()
        {
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(node_iter.getNode().u < 0)
                    return false;
            }
            return true;
        }

        virtual double getOcupition()
        {
            double base(0);
            for(typename NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode().u;
            return base;
        }
};

class AlleeRK4NormalC : public AlleeRK4VaryCBase<AlleePatchRK4VaryC>
{
    public:
        double c_mu; // mean for allee_threshold
        double c_sigma; // sd for allee_threshold
        mylib::RandNormal rd;

        AlleeRK4NormalC(double mu, double sigma, double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init): AlleeRK4VaryCBase<AlleePatchRK4VaryC>(mu_ratio, h, graph, steps, epsilon, init), c_mu(mu), c_sigma(sigma), rd(c_mu, c_sigma)
        {
            initialize();
        }

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                node_iter.getNode().h = interval;
                node_iter.getNode().c = rd.gen();
            }
        }
};


class AlleeRK4BiC : public AlleeRK4VaryCBase<AlleePatchRK4VaryC>
{
    public:
        double c_low; // the lower c
        double c_high; // the higher c
        double c_ratio; // two cs' ratio
        mylib::RandDouble rd;

        AlleeRK4BiC(double clow, double chigh, double cratio, double mu_ratio, double h, PUNGraph graph, int steps, double epsilon, double init): AlleeRK4VaryCBase<AlleePatchRK4VaryC>(mu_ratio, h, graph, steps, epsilon, init), c_low(clow), c_high(chigh), c_ratio(cratio), rd()
        {
            initialize();
        }

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                node_iter.getNode().h = interval;
                if(rd.gen() < c_ratio)
                {
                    node_iter.getNode().c = c_high;
                }
                else
                {
                    node_iter.getNode().c = c_low;
                }

            }
        }
};

class AlleeRK4GridC : public AlleeRK4VaryCBase<AlleePatchRK4VaryC>
{
    public:
        int grid_length;
        double c;
        
        AlleeRK4GridC(double l, double allee_threshold, PUNGraph graph, double mu_ratio, double h, int steps, double epsilon, double init): AlleeRK4VaryCBase<AlleePatchRK4VaryC>(mu_ratio, h, graph, steps, epsilon, init), grid_length(l), c(allee_threshold)
        {
            assert(graph->GetNodes() == grid_length * grid_length);
            initialize();
        }

        virtual void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                node_iter.getNode().h = interval;
                node_iter.getNode().c = c;
            }
        }

        void profile(std::ostream & out)
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                out << node_iter->GetId() << " ";
                for(int i=0; i<node_iter->GetDeg(); ++i)
                {
                    NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                    out << neighbor_iter->GetId() << " ";
                }
                out << std::endl;
            }
        }

        void dumpSnap(std::ostream & out)
        {
            for(int i=0; i<net_size; ++i)
            {
                out << net.getNodeFromId(i).getNode().u << " ";
                if((i+1) % grid_length == 0)
                    out << std::endl;
            }
        }


};

#endif
